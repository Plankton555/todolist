package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import model.ItemToDo;
import model.ToDoModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class ToDoListApp implements Observer {

	private static final String FILE_PATH = "data";

	private JFrame toDoListFrame;
	private JTextField textField;
	private JSlider slider;
	private ToDoModel model;
	private JList<String> list;
	private JButton btnDelete;
	private JButton btnReset;
	private JButton btnUpdate;
	private JLabel lblDate;
	private JButton btnPickAnItem;


	/**
	 * Create the application.
	 */
	public ToDoListApp() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "serial", "unchecked", "rawtypes" })
	private void initialize() {
		model = new ToDoModel();
		model.addObserver(this);
		model.loadData(FILE_PATH);

		toDoListFrame = new JFrame();
		toDoListFrame.setTitle(model.getSubjectTitle()
				+ " - Plankter Productions");
		toDoListFrame.setBounds(100, 100, 550, 500);
		toDoListFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		toDoListFrame.getContentPane().setLayout(null);
		toDoListFrame.setLocationRelativeTo(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Actions", TitledBorder.LEADING,
				TitledBorder.TOP, null, null));
		panel.setBounds(260, 374, 264, 56);
		toDoListFrame.getContentPane().add(panel);

		btnDelete = new JButton("Delete");
		btnDelete.setEnabled(false);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> itemNames = list.getSelectedValuesList();
				if (!itemNames.isEmpty()) {
					model.removeItem(itemNames.get(0));
					resetTextAndPrio();
				}
			}
		});
		panel.add(btnDelete);

		btnReset = new JButton("Reset");
		btnReset.setEnabled(false);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				List<String> itemNames = list.getSelectedValuesList();
				if (!itemNames.isEmpty()) {
					model.resetItem(itemNames.get(0));
				}
			}
		});
		panel.add(btnReset);

		btnUpdate = new JButton("Update");
		btnUpdate.setEnabled(false);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<String> itemNames = list.getSelectedValuesList();
				if (!itemNames.isEmpty()) {
					model.updateItem(itemNames.get(0), textField.getText(),
							slider.getValue());
					resetTextAndPrio();
				}
			}
		});
		panel.add(btnUpdate);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Properties",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(260, 45, 264, 274);
		toDoListFrame.getContentPane().add(panel_1);
		panel_1.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(6, 16, 252, 30);
		panel_1.add(panel_2);
		panel_2.setLayout(null);

		JLabel lblName = new JLabel("Name: ");
		lblName.setBounds(10, 8, 65, 14);
		panel_2.add(lblName);
		lblName.setHorizontalAlignment(SwingConstants.CENTER);

		textField = new JTextField();
		textField.setBounds(85, 5, 157, 20);
		panel_2.add(textField);
		textField.setColumns(10);

		JPanel panel_3 = new JPanel();
		panel_3.setBounds(6, 57, 252, 123);
		panel_1.add(panel_3);
		panel_3.setLayout(null);

		JLabel lblRating = new JLabel("Priority");
		lblRating.setBounds(10, 11, 232, 14);
		panel_3.add(lblRating);
		lblRating.setHorizontalAlignment(SwingConstants.CENTER);

		slider = new JSlider();
		slider.setMaximum(50);
		slider.setBounds(24, 53, 200, 23);
		panel_3.add(slider);
		slider.setValue(30);
		slider.setMinimum(10);

		JLabel lblLow = new JLabel("Low");
		lblLow.setHorizontalAlignment(SwingConstants.CENTER);
		lblLow.setBounds(10, 75, 56, 14);
		panel_3.add(lblLow);

		JLabel lblMedium = new JLabel("Medium");
		lblMedium.setHorizontalAlignment(SwingConstants.CENTER);
		lblMedium.setBounds(76, 75, 95, 14);
		panel_3.add(lblMedium);

		JLabel lblHigh = new JLabel("High");
		lblHigh.setHorizontalAlignment(SwingConstants.CENTER);
		lblHigh.setBounds(181, 75, 61, 14);
		panel_3.add(lblHigh);

		JPanel panel_4 = new JPanel();
		panel_4.setBounds(6, 220, 252, 47);
		panel_1.add(panel_4);
		panel_4.setLayout(null);

		JLabel lblDateAdded = new JLabel("Date added: ");
		lblDateAdded.setHorizontalAlignment(SwingConstants.CENTER);
		lblDateAdded.setBounds(10, 11, 109, 25);
		panel_4.add(lblDateAdded);

		lblDate = new JLabel("");
		lblDate.setBounds(129, 11, 113, 25);
		panel_4.add(lblDate);

		JPanel panel_5 = new JPanel();
		panel_5.setBounds(10, 11, 240, 23);
		toDoListFrame.getContentPane().add(panel_5);
		panel_5.setLayout(new GridLayout(0, 2, 10, 0));

		JButton btnAddNew = new JButton("Add new");
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				model.addItem(textField.getText(), slider.getValue());
				resetTextAndPrio();
			}
		});
		panel_5.add(btnAddNew);

		btnPickAnItem = new JButton("Pick an item");
		btnPickAnItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object[] options = { "Cancel", "Pick another one",
						"Take this one" };
				ItemToDo pickedItem = null;
				int answer = 1;
				while (answer == 1) {
					pickedItem = model.pickRndRoulette();
					if (pickedItem == null) {
						answer = 0;
					} else {
						answer = JOptionPane.showOptionDialog(
								null,
								"<html>" + "I picked this one for you: <b>" + pickedItem.getName() + "</b></html>", "Picked Item",
								JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE, null, options,
								options[2]);
					}
				}
				if (answer == 2) {
					model.removeItem(pickedItem);
				}
			}
		});
		panel_5.add(btnPickAnItem);

		JPanel panel_6 = new JPanel();
		panel_6.setBounds(260, 11, 264, 30);
		toDoListFrame.getContentPane().add(panel_6);

		JLabel lblSelectedItem = new JLabel("Selected item");
		panel_6.add(lblSelectedItem);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 45, 240, 385);
		toDoListFrame.getContentPane().add(scrollPane);
		
				list = new JList<String>();
				scrollPane.setViewportView(list);
				list.addListSelectionListener(new ListSelectionListener() {
					public void valueChanged(ListSelectionEvent arg0) {
						int index = list.getSelectedIndex();
						if (index >= 0) {
							setActionButtonsenabled(true);
							ItemToDo item = model.getItem(index);
							textField.setText(item.getName());
							slider.setValue(item.getPriority());

							DateFormat dateFormat = DateFormat
									.getDateInstance(DateFormat.SHORT);
							String dateString = dateFormat.format(new Date(item
									.getTimeAdded()));
							lblDate.setText(dateString);

							// System.out.println("Selected item: " + item);
						} else if (index == -1) {
							setActionButtonsenabled(false);
						}
					}
				});
				list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
				list.setModel(new AbstractListModel() {
					String[] values = new String[] {};

					public int getSize() {
						return values.length;
					}

					public Object getElementAt(int index) {
						return values[index];
					}
				});
				list.setBorder(null);

		JMenuBar menuBar = new JMenuBar();
		toDoListFrame.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmChangeTitle = new JMenuItem("Change title");
		mntmChangeTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String title = JOptionPane.showInputDialog(null,
						"Enter a new window title", "Choose new title", 1);
				if (title != null) {
					model.setSubjectTitle(title);
				}
			}
		});
		mnFile.add(mntmChangeTitle);

		JMenu mnEdit = new JMenu("Edit");
		menuBar.add(mnEdit);

		JMenu mnView = new JMenu("View");
		menuBar.add(mnView);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		update(model, null);
	}

	protected void setActionButtonsenabled(boolean b) {
		btnDelete.setEnabled(b);
		btnReset.setEnabled(b);
		btnUpdate.setEnabled(b);
	}

	protected void resetTextAndPrio() {
		textField.setText("");
		slider.setValue((slider.getMaximum() + slider.getMinimum()) / 2);
	}

	public void setVisible(boolean b) {
		this.toDoListFrame.setVisible(b);
	}

	@SuppressWarnings("serial")
	@Override
	public void update(Observable arg0, Object arg1) {
		List<ItemToDo> items = model.getAllListItems();
		final String[] itemNames = new String[items.size()];
		for (int i = 0; i < items.size(); i++) {
			itemNames[i] = items.get(i).getName();
		}

		list.setModel(new AbstractListModel<String>() {

			@Override
			public String getElementAt(int arg0) {
				return itemNames[arg0];
			}

			@Override
			public int getSize() {
				return itemNames.length;
			}
		});
		btnPickAnItem.setEnabled(list.getModel().getSize() > 0);
		toDoListFrame.setTitle(model.getSubjectTitle()
				+ " - Plankter Productions");
		saveData();
	}

	private void saveData() {
		model.saveData(FILE_PATH);
	}
}
