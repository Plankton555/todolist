package model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class ItemToDo implements Comparable<ItemToDo> {

	private static final double MILLISEC_PER_DAY = 86400000;
	@Attribute
	private String name;
	@Element
	private int priority;
	@Element
	private long timeAdded;

	// TODO Write javadoc
	public ItemToDo() {
	}

	public ItemToDo(String name, int priority, long timeAdded) {
		this.name = name;
		this.priority = priority;
		this.timeAdded = timeAdded;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * @return the timeAdded
	 */
	public long getTimeAdded() {
		return timeAdded;
	}

	/**
	 * @param timeAdded
	 *            the timeAdded to set
	 */
	public void setTimeAdded(long timeAdded) {
		this.timeAdded = timeAdded;
	}

	public double getValue() {
		return this.getPriority()
				* ((System.currentTimeMillis() - this.getTimeAdded()) / MILLISEC_PER_DAY);
	}

	@Override
	public int compareTo(ItemToDo o) {
		return (int) Math.signum(o.getValue() - this.getValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 191;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemToDo other = (ItemToDo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemToDo [name=" + name + ", priority=" + priority
				+ ", timeAdded=" + timeAdded + ", currentValue=" + getValue()
				+ "]";
	}
}