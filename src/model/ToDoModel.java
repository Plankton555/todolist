package model;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

@Root
public class ToDoModel extends Observable {

	@Attribute
	private String subjectTitle = "ToDoList";

	@ElementList
	private List<ItemToDo> items = new ArrayList<ItemToDo>();

	/**
	 * Adds an item to the model.
	 * 
	 * @param name
	 *            Name of the item
	 * @param priority
	 *            Priority of the item
	 * @return false if an item with the same name already exists, true if the
	 *         item is unique and could be created
	 */
	public boolean addItem(String name, int priority) {
		if (name.equals("")) {
			return false;
		}

		ItemToDo newItem = new ItemToDo(name, priority,
				System.currentTimeMillis());
		if (items.contains(newItem)) {
			return false;
		} else {
			items.add(newItem);
			setModelChanged();
			return true;
		}
	}

	/**
	 * Removes the item with the provided name if it exists. Otherwise nothing
	 * will happen.
	 * 
	 * @param name
	 *            Name of the item to remove
	 */
	public void removeItem(String name) {
		removeItem(new ItemToDo(name, 0, 0));
	}

	/**
	 * Removes the item if it exists. Otherwise nothing will happen.
	 * 
	 * @param item
	 *            Item to remove
	 */
	public void removeItem(ItemToDo item) {
		if (items.remove(item)) {
			setModelChanged();
		}
	}

	/**
	 * Resets the timeAdded to 'now' if the item with the provided name exists.
	 * Otherwise nothing will happen.
	 * 
	 * @param name
	 *            Name of the item to reset
	 */
	public void resetItem(String name) {
		for (ItemToDo i : items) {
			if (i.getName().equals(name)) {
				i.setTimeAdded(System.currentTimeMillis());
				setModelChanged();
				break;
			}
		}
	}

	/**
	 * Resets the timeAdded to 'now' if the item exists. Otherwise nothing will
	 * happen.
	 * 
	 * @param item
	 *            Item to reset
	 */
	public void resetItem(ItemToDo item) {
		resetItem(item.getName());
	}

	/**
	 * Picks an item from the model using the roulette wheel selection method.
	 * 
	 * @return Picked item
	 */
	public ItemToDo pickRndRoulette() {
		Collections.sort(items);
		double total = 0;
		for (ItemToDo i : items) {
			total += i.getValue();
		}
		double pickedValue = Math.random() * total;
		int pickedIndex = -1;

		for (int i = 0; i < items.size(); i++) {
			pickedValue -= items.get(i).getValue();
			if (pickedValue < 0) {
				pickedIndex = i;
				break;
			}
		}
		if (pickedIndex < 0 || pickedIndex >= items.size()) {
			// System.out.println("Invalid index in ItemToDo.prickRndRoulette()");
			return null;
		}
		return items.get(pickedIndex);
	}

	/**
	 * @param subjectTitle
	 *            Subject title
	 */
	public void setSubjectTitle(String subjectTitle) {
		this.subjectTitle = subjectTitle;
		setModelChanged();
	}

	/**
	 * @return Subject title
	 */
	public String getSubjectTitle() {
		return subjectTitle;
	}

	/**
	 * Loads data from file.
	 * 
	 * @param filePath
	 *            The folder path of the file to load. If no file is found, a
	 *            new and empty data base will be created.
	 */
	public void loadData(String filePath) {
		Serializer serializer = new Persister();
		File source = new File(filePath + "\\tododata.xml");
		ToDoModel loadedModel = null;
		try {
			new File(filePath).mkdirs();
			if (!source.createNewFile())
			{
				loadedModel = serializer.read(ToDoModel.class, source);
				this.subjectTitle = loadedModel.subjectTitle;
				this.items = loadedModel.items;
			}
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		Collections.sort(this.items);
	}

	/**
	 * Saves data to file
	 * 
	 * @param filePath
	 *            The path where the file is saved. Any old file at that path
	 *            will be overwritten.
	 */
	public void saveData(String filePath) {
		Serializer serializer = new Persister();
		File result = new File(filePath + "\\tododata.xml");

		new File(filePath).mkdirs();
		try {
			serializer.write(this, result);
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<ItemToDo> getAllListItems() {
		List<ItemToDo> output = new ArrayList<ItemToDo>();
		for (ItemToDo i : items) {
			output.add(i);
		}
		Collections.sort(output);
		return output;
	}

	public boolean updateItem(String changingItem, String newName,
			int newPriority) {
		if (newName.equals("")) {
			return false; // We can't have an empty name, can we?
		}
		ItemToDo itemToBeChanged = null;
		for (ItemToDo i : items) {
			if (i.getName().equals(newName) && !newName.equals(changingItem)) {
				return false; // There is already an item with that name
			}
			if (i.getName().equals(changingItem)) {
				itemToBeChanged = i;
			}
		}
		itemToBeChanged.setName(newName);
		itemToBeChanged.setPriority(newPriority);

		setModelChanged();
		return true;
	}

	private void setModelChanged() {
		Collections.sort(items);
		setChanged();
		notifyObservers();
	}

	public ItemToDo getItem(int i) {
		return items.get(i);
	}
}